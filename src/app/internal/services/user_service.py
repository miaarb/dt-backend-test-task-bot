from django.core.exceptions import ObjectDoesNotExist

from app.internal.models.user import User

class PhoneNotFilledException(Exception):
    pass


async def create_or_update_user(id: int, username: str) -> None:
    await User.objects.aupdate_or_create(id=id, defaults={"username": username})


async def try_get_user_info(id: int) -> dict:
    try:
        user = await User.objects.aget(id=id)
    except ObjectDoesNotExist:
        raise ValueError("Unknown user id")

    if user.phone is None:
        raise PhoneNotFilledException

    return {
        "id": user.id,
        "username": user.username,
        "phone": (user.phone),
    }


async def try_set_phone(id: int, phone: str) -> None:
    try:
        await User.objects.filter(id=id).aupdate(phone=phone)
    except ObjectDoesNotExist:
        raise ValueError("Unknown user id")
