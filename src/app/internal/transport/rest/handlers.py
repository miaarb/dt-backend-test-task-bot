from asgiref.sync import async_to_sync
from django.http import JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view

from app.internal.services.user_service import PhoneNotFilledException, try_get_user_info


@api_view(["GET"])
def get_user_info(request, user_id):
    try:
        try_get_user_info_sync = async_to_sync(try_get_user_info)
        return JsonResponse(try_get_user_info_sync(user_id), status=status.HTTP_200_OK)
    except ValueError:
        return JsonResponse({"error": "User not found"}, status=status.HTTP_404_NOT_FOUND)
    except PhoneNotFilledException:
        return JsonResponse({"error": "Phone not filled"}, status=status.HTTP_400_BAD_REQUEST)
