from telegram import KeyboardButton, ReplyKeyboardMarkup, Update
from telegram.ext import ContextTypes

from app.internal.services.user_service import (
    PhoneNotFilledException,
    create_or_update_user,
    try_get_user_info,
    try_set_phone,
)

from .messages_text import (
    START_ANSWER,
    PHONE_NOT_FILLED_ANSWER,
    PHONE_SAVED_ANSWER,
    REQUEST_PHONE_ANSWER,
    HELP_ANSWER,
    SEND_CONTACT_BUTTON_TEXT,
    get_user_info_answer,
)


async def handle_error(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    if isinstance(context.error, PhoneNotFilledException):
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=PHONE_NOT_FILLED_ANSWER,
        )
    else:
        print(context.error)


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await create_or_update_user(update.effective_user.id, update.effective_user.username)
    await context.bot.send_message(
        chat_id=update.effective_chat.id, text=START_ANSWER
    )


async def me(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    user_info = await try_get_user_info(update.effective_user.id)
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=get_user_info_answer(user_info),
    )


async def set_phone(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    reply_markup = ReplyKeyboardMarkup(
        [[KeyboardButton(text=SEND_CONTACT_BUTTON_TEXT, request_contact=True)]], one_time_keyboard=True
    )
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=REQUEST_PHONE_ANSWER,
        reply_markup=reply_markup,
    )


async def set_contact(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await try_set_phone(update.effective_user.id, update.effective_message.contact.phone_number)
    await context.bot.send_message(chat_id=update.effective_chat.id, text=PHONE_SAVED_ANSWER)


async def help(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=HELP_ANSWER,
    )


handlers = {
    "start": start,
    "me": me,
    "set_phone": set_phone,
    "help": help,
}
