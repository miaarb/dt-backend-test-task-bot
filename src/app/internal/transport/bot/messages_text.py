PHONE_NOT_FILLED_ANSWER = "Сначала нужно заполнить номер телефона"
START_ANSWER = "Привет! Зарегистрировал пользователя. /help - посмотреть все команды"
HELP_ANSWER = """
    Доступные команды:
    /start - запустить бота и сохранить инфо о себе
    /me - посмотреть инфо о себе
    /set_phone - сохранить номер телефона
    """
PHONE_SAVED_ANSWER = "Сохранили твой номер"
REQUEST_PHONE_ANSWER = "Нажми на кнопку 'Отправить контакт', чтобы я мог получить твой номер телефона"
SEND_CONTACT_BUTTON_TEXT = "Отправить контакт"


def get_user_info_answer(user_info: dict) -> str:
    return f"Инфо о тебе: твой id: {user_info['id']}, твой ник: @{user_info['username']}, твой телефон: {user_info['phone']}"
